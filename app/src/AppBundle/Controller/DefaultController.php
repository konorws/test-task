<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="listpage")
     * @Template(template="@App/list.html.twig")
     */
    public function indexAction()
    {
        $userService = $this->get('app.user');
        $entityManager = $this->getDoctrine();
        $postRepository = $entityManager->getRepository(Post::class);

        return [
            'posts' => $postRepository->findAll(),
            'addCommentAction' => $this->generateUrl("comment_add"),
            'authStatus' => $userService->checkAuth(),
        ];
    }

    /**
     * @Template(template="@App/authForm.html.twig")
     */
    public function authFormAction()
    {
        $userService = $this->get("app.user");

        return [
            'authStatus' => $userService->checkAuth(),
            'action' => $this->generateUrl("auth")
        ];
    }

    /**
     * @Template(template="@App/postForm.html.twig")
     */
    public function postFormAction()
    {
        $userService = $this->get("app.user");

        return [
            'authStatus' => $userService->checkAuth(),
            'action' => $this->generateUrl("post_add")
        ];

    }

    /**
     * @Route("/auth", name="auth")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function authAction(Request $request)
    {
        $redirectTo = "listpage";

        $userName = $request->get('user_name');
        if(empty($userName)) {
            $this->addFlash("danger", "Invalid user name");
            return $this->redirectToRoute($redirectTo);
        }

        $userService = $this->get("app.user");
        $userId = $userService->addUser($userName);
        setcookie("user_id", $userId, time() + 86000);
        $this->addFlash("success", "You success auth as ".$userName);

        return $this->redirectToRoute($redirectTo);
    }

    /**
     * @Route("/post_add", name="post_add")
     */
    public function addPostAction(Request $request)
    {
        $redirectTo = "listpage";

        $title = $request->get('title');
        $content = $request->get('content');
        if(empty($title) || empty($content)) {
            $this->addFlash("danger", "Invalid form data");
            return $this->redirectToRoute($redirectTo);
        }

        $userService = $this->get("app.user");
        if(!$userService->checkAuth()) {
            $this->addFlash("danger", "You not access");
            return $this->redirectToRoute($redirectTo);
        }


        $post = new Post();
        $post->setTitle($title);
        $post->setContent($content);
        $post->setUser($userService->getUser());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($post);
        $entityManager->flush();

        $this->addFlash("success", "You post success added");

        return $this->redirectToRoute($redirectTo);
    }

    /**
     * @Route("/comment_add", name="comment_add")
     */
    public function addCommentAction(Request $request)
    {
        $redirectTo = "listpage";

        $postId = $request->get('post');
        $content = $request->get('content');

        if(empty($postId) || empty($content)) {
            $this->addFlash("danger", "Invalid form data");
            return $this->redirectToRoute($redirectTo);
        }

        $userService = $this->get("app.user");
        if(!$userService->checkAuth()) {
            $this->addFlash("danger", "You not access");
            return $this->redirectToRoute($redirectTo);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $postRepository = $entityManager->getRepository(Post::class);
        /** @var Post $post */
        $post = $postRepository->find($postId);

        if(empty($post)) {
            $this->addFlash("danger", "Invalid post id");
            return $this->redirectToRoute($redirectTo);
        }

        $comment = new Comment();
        $comment->setUser($userService->getUser());
        $comment->setPost($post);
        $comment->setContent($content);

        $entityManager->persist($comment);
        $entityManager->flush();


        $this->addFlash("success", "You post success added");

        return $this->redirectToRoute($redirectTo);
    }
}
