<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;


/**
 * Simple user service. For authorization stub
 *
 * Class UserService
 *
 * @package AppBundle\Service
 */
Class UserService
{
    private $user;

    private $entityManager;

    /**
     * UserService constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

        // Check is auth
        if(isset($_COOKIE['user_id'])) {
            $this->user = $this->getUserById($_COOKIE['user_id']);
        }
    }

    /**
     * Check is user auth
     *
     * @return bool
     */
    public function checkAuth()
    {
        return !empty($this->user);
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * Return user id or null
     *
     * @return bool
     */
    public function getUserId()
    {
        if(!empty($this->user)) {
            return  $this->user->getId();
        }

        return null;
    }

    /**
     * @param string $userName
     * @return int|bool
     */
    public function addUser($userName)
    {
        $user = new User();
        $user->setName($userName);

        $this->entityManager->persist($user);
        try {
            $this->entityManager->flush();
        } catch (ORMException $e) {
            return false;
        }

        return $user->getId();
    }

    /**
     * @param $user_id
     *
     * @return User|null
     */
    public function getUserById($user_id)
    {
        try {
            /** @var User $user */
            $user = $this->entityManager->find(User::class, $user_id);

            return $user;
        } catch (ORMException $e) {
            return NULL;
        }
    }
}
